from django.apps import AppConfig


class CustperapiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'custperapi'
